Place "DONE" before line when finished.
Place "WORKING" before line if working on that line

KA DONE: 1. (p.7) To ekstra screenshots som illustrerer QuickFind og QuickUnion, samme komponentstruktur, men ulik høyde av trærne.

KA DONE: 2. (p. 7) connected(_,_) tar inn noder, ikke komponenter.

KA DONE: 3. Ekstra screenshot med Quicksort "trapp" (p. 10)

KA DONE: 4. Figur 3.3 kan med fordel byttes ut med en etter at noen noder har blitt lagt til men ikke kanter (tom graf er lite informativ).

KA DONE: 5. Samme merknad gjelder Figur 3.5. Figur 3.6 virker i første omgang feil fordi det vises kanter, men ikke nabolister. Blir riktig etter footnote 1). Men underskrift av Figur 3.6 er feil (kan gjenbrukes for 3.5 dersom dere følger mitt råd). Derfor kanskje best å fylle 3.5 med graf representasjon +  nabolister, for så å si at de kan skjules, med underskrift fra 3.6.

KA DONE: 7. Forslag til underskrift 3.6: "halvveis BFS"

KA DONE: 6. Ekstra screenshot "halvveis DFS" kunne vært nyttig

7. Skriv i starten hvor programmet kan nedlastes/kjøres (sensor bør kunne gjøre det).

8. Kan vi på sikt opprette en engelskspråkelig webside for videre distribusjon?
