# README #

### OS X users ###
Open your terminal and move to preferred directory. To download jar-file type in: 

```
#!bash

curl -LOk https://bitbucket.org/algoritmevisualisering/algoritmevisualisering/downloads/algoritmevisualisering.jar
```

### Windows users
Download the file from Downloads -> algoritmevisualisering.jar). Move the file to preferred directory. Open terminal and navigate to that directory.

### Starting the program (all OS'es) ###

Start the program from terminal using this command: 
```
#!bash

java -jar algoritmevisualisering.jar
```

*NB! Requires JDK 1.8.91 or newer! *

### What is this repository for? ###

This is a graphical animated visualization for these algorithms:

* UnionFind (with variations)
* InsertionSort
* QuickSort
* How graphs are implemented
* DFS & BFS

### How do I get set up? ###

* There is nothing to set up :)

### Contribution guidelines ###

* Come to lesesalen.

### Who do I talk to? ###

* Ragnhild Aalvik, Kristian Rosland or Knut Anders Stokke