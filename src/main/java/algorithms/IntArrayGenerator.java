package algorithms;

import algorithms.SimpleSorting.IsSorted;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

/**
 * Class for generating random int arrays, use for testing and autofill
 *
 * Created by KnutAnders
 */
public class IntArrayGenerator {
    public enum ARRAY_TYPE { RANDOM, ALMOST_SORTED, SORTED, INVERTED_SORTED };

    public static Comparator<Integer> reversedComparator;

    static {
        reversedComparator = ((n, m) -> {
            if (n > m) return -1;
            if (m > n) return 1;
            else return 0;
        });
    }

    public int[] getArray(Random r, int size) {
        if (size < 1)
            throw new IllegalArgumentException("Size must be at least 1");

        int[] array = new int[size];
        for (int i=0; i<size; i++)
            array[i] = r.nextInt(100);

        return array;
    }

    public int[] getArray(int size) {
        Random r = new Random();
        return getArray(r, size);
    }

    public Integer[] getIntegerArray(int size) {
        int[] array = getArray(size);
        Integer[] newArray = new Integer[array.length];
        for (int i=0; i<array.length; i++)
            newArray[i] = array[i];
        return newArray;
    }

    public int [] integerToIntArray(Integer [] arr) {
        int [] newArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++)
            newArr[i] = arr[i];
        return newArr;
    }

    public Integer[] getIntegerArray(int size, ARRAY_TYPE type) {
        Integer [] arr = getIntegerArray(size);
        if (type == ARRAY_TYPE.SORTED) {
            Arrays.sort(arr);
            return arr;
        } else if (type == ARRAY_TYPE.INVERTED_SORTED) {
            Arrays.sort(arr, reversedComparator);
            return arr;
        } else if (type == ARRAY_TYPE.ALMOST_SORTED) {
            return getAlmostSortedArray(arr);
        } else
            return arr;
    }

    private Integer[] getAlmostSortedArray(Integer [] arr) {
        Arrays.sort(arr);
        Random rand = new Random();
        for (int i = 1; i < arr.length-1; i++) {
            if (rand.nextDouble() < 0.70) {
                if (rand.nextDouble() < 0.5) {
                    int temp = arr[i]; arr[i] = arr[i+1]; arr[i+1] = temp;
                } else {
                    int temp = arr[i]; arr[i] = arr[i-1]; arr[i-1] = temp;
                }
            }
        }

        if (IsSorted.isSorted(arr)) { return getAlmostSortedArray(arr); } //To avoid a sorted array

        return arr;
    }

}
