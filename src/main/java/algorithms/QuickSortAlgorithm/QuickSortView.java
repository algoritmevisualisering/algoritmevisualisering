package algorithms.QuickSortAlgorithm;

import algorithms.IntArrayGenerator;
import algorithms.SimpleSorting.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static algorithms.webViewUtils.Java2JavascriptUtils.connectBackendObject;

/**
 * This supreme motherfucking class was created by the legend kristianrosland on 06.09.2016.
 */
public class QuickSortView extends Application {
    private WebView webView;
    private final double screenHeight = 700;
    private final double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
    private final int ARR_LENGTH = 13;
    private final String[] colors = {"#7FFF00", "not used", "#FFB366"}; //Green and orange
    private QuickSortAlgorithm quickSort;
    private IntArrayGenerator arrayGenerator = new IntArrayGenerator();
    private EventManager eventManager;
    private int lastLeftArrowIndex = 0, lastRightArrowIndex = 0;
    private int lastLeftArrowNr = 0, lastRightArrowNr = 0;
    private boolean paused = true, isReset = true;

    public static void main(String[] args) {
        System.setProperty("prism.lcdtext", "false"); // enhance fonts
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        createWindow(primaryStage, "/resForQuickSort/QuickSort.html");
    }

    private void createWindow(Stage primaryStage, String path) {
        webView = new WebView();
        eventManager = new EventManager(webView.getEngine());
        eventManager.setDelay(2000);

        // Use connectBackEndObject to connect an object to a js variable
        connectBackendObject(webView.getEngine(), "javaBinder", this);

        webView.getEngine().setOnAlert((event) -> System.out.println("WebView: " + event.getData()));

        primaryStage.setOnCloseRequest((i) -> System.exit(0));

        webView.setPrefSize(screenWidth, screenHeight);
        webView.getEngine().load(getClass().getResource(path).toExternalForm());
        webView.setContextMenuEnabled(false);

        primaryStage.setScene(new Scene(webView));
        primaryStage.setTitle("Quick Sort");
        primaryStage.show();

        quickSort = new QuickSortAlgorithm(this);
    }

    public void moveArrayElementToIndex(int fromIndex, int toIndex) {
        List<String> forwardSteps = new ArrayList<>();
        forwardSteps.add("setPosition(" + fromIndex + ", " + toIndex * 70 + ", 0)");
        forwardSteps.add("swapId(" + fromIndex + ", " + toIndex + ")");

        List<String> backwardSteps = new ArrayList<>();
        backwardSteps.add("swapId(" + toIndex + ", " + fromIndex + ")");
        backwardSteps.add("setPosition(" + fromIndex + ", " + fromIndex * 70 + ", 0)");

        eventManager.addEvent(new FrontEndEvent(forwardSteps, backwardSteps, FrontEndEvent.NORMAL));
    }


    public void setValueInArray(int index, int value) {}

    private JSONArray jsonConvert(Comparable [] arr) {
        try {
            return new JSONArray(arr);
        } catch (JSONException jsonEx) {
            System.out.println(jsonEx.getMessage());
            return null;
        }
    }

    public void setColorInArrayElement(int index, int color, boolean colorOn) {
        eventManager.addEvent(new FrontEndEvent(
                Arrays.asList("setColor(" + index + ", '" + colors[color] + "', " + colorOn + ")"),
                Arrays.asList("setColor(" + index + ", '" + colors[color] + "', " + !colorOn + ")"),
                FrontEndEvent.NO_DELAY) //NO-DELAY to avoid 'lag' when selecting
        );
    }

    public void setArray(Integer [] array) {
        quickSort.setArray(array);
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("setArray('" + jsonConvert(array).toString() + "')"), null, FrontEndEvent.NO_DELAY));
    }


    public void setRandomArray() {
        isReset = true;
        Integer [] arr = arrayGenerator.getIntegerArray(ARR_LENGTH);

        eventManager.clear();
        setLevelHeight(30);
        setArray(arr);
        unpause();

        clearScreen();
    }

    public void setAlmostSortedArray() {
        isReset = true;
        Integer [] arr = arrayGenerator.getIntegerArray(ARR_LENGTH, IntArrayGenerator.ARRAY_TYPE.ALMOST_SORTED);
        eventManager.clear();
        setLevelHeight(15);
        setArray(arr);
        unpause();

        clearScreen();
    }

    public void setSortedArray() {
        Integer [] arr = arrayGenerator.getIntegerArray(ARR_LENGTH, IntArrayGenerator.ARRAY_TYPE.SORTED);
        eventManager.insertPauseIcon();
        isReset = true;
        eventManager.clear();
        setLevelHeight(15);
        setArray(arr);
        unpause();

        clearScreen();
    }

    public void setInvertedSortedArray() {
        Integer [] arr = arrayGenerator.getIntegerArray(ARR_LENGTH, IntArrayGenerator.ARRAY_TYPE.INVERTED_SORTED);
        eventManager.insertPauseIcon();
        isReset = true;
        eventManager.clear();
        setLevelHeight(15);
        setArray(arr);
        unpause();

        clearScreen();
    }

    private void clearScreen() {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("clearScreen();"),
                null,
                FrontEndEvent.NO_DELAY
        );
        eventManager.addEvent(event);
    }


    public void lockControllers() {
        // eventManager.addEvent(new FrontEndEvent(Arrays.asList("disableControllers()"), null, FrontEndEvent.NO_DELAY));
    }

    public void enableButtons() {
        // eventManager.addEvent(new FrontEndEvent(Arrays.asList("enableControllers()"), null, FrontEndEvent.NO_DELAY));
    }

    private void executeScript(String script) {
        Platform.runLater(() -> webView.getEngine().executeScript(script));
    }

    public void startQuickSort() {
        quickSort.sort();
    }

    public void togglePause() {
        if (isReset) {
            isReset = false;
            quickSort.sort();
        } else {
            eventManager.togglePause();
        }
    }
    private void unpause() {
        eventManager.unpause();
    }
    public void forward() {
        eventManager.nextEvent();
    }
    public void backward() {
        eventManager.oneStepBack();
    }


    public void setIIndex(int i) {
        // TODO implement
    }

    public void setJIndex(int j) {
        // TODO implement
    }

    public void lowerElements(int lo, int hi) {
        if (lo > hi) return;

        FrontEndEvent event = new FrontEndEvent(Arrays.asList("lowerElements(" + lo + ", " + hi + ");"), Arrays.asList("liftElements(" + lo + ", " + hi + ");"), FrontEndEvent.NORMAL);
        eventManager.addEvent(event);
    }

    public void liftElements(int lo, int hi) {
        if (lo > hi) return;

        FrontEndEvent event = new FrontEndEvent(Arrays.asList("liftElements(" + lo + ", " + hi + ");"), Arrays.asList("lowerElements(" + lo + ", " + hi + ");"), FrontEndEvent.NORMAL);
        eventManager.addEvent(event);
    }

    public void swapElements(int i, int j) {
        FrontEndEvent event = new FrontEndEvent(Arrays.asList("swapElements(" + i + ", " + j + ");"), Arrays.asList("swapElements(" + i + ", " + j + ");"), FrontEndEvent.NORMAL);
        eventManager.addEvent(event);

    }
    public void print(String msg) {
        System.out.println("WebView: " + msg);
    }

    public void setPivotElement(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("selectPivotElement(" + index + ");"),
                Arrays.asList("deselectPivotElement(" + index + ");"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);
    }

    public void deselectPivotElement(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("deselectPivotElement(" + index + ");"),
                Arrays.asList("selectPivotElement(" + index + ");"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);
    }

    public void setLeftAndRightArrow(int left, int right) {
        if (lastLeftArrowIndex == left && lastRightArrowIndex == right) return;

        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("moveLeftArrowToIndex(" + left + ", 0);", "moveRightArrowToIndex(" + right + ", 0);"),
                Arrays.asList("moveLeftArrowToIndex(" + lastLeftArrowIndex + ", 0);", "moveRightArrowToIndex(" + lastRightArrowIndex + ", 0);"),
                FrontEndEvent.NO_DELAY);
        eventManager.addEvent(event);
        lastLeftArrowIndex = left;
        lastRightArrowIndex = right;
    }

    public void showArrows() {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("showLeftArrow();", "showRightArrow();"),
                Arrays.asList("hideLeftArrow();", "hideRightArrow();"),
                FrontEndEvent.NO_DELAY
        );
        eventManager.addEvent(event);
    }

    public void hideArrows() {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("hideLeftArrow();", "hideRightArrow();"),
                Arrays.asList("showLeftArrow();", "showRightArrow();"),
                FrontEndEvent.NO_DELAY
        );
        eventManager.addEvent(event);
    }

    public void moveLeftArrow(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("moveLeftArrowToIndex(" + index + ", 1000);"),
                Arrays.asList("moveLeftArrowToIndex(" + lastLeftArrowIndex + ", 1000);"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);
        lastLeftArrowIndex = index;
    }

    public void moveRightArrow(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("moveRightArrowToIndex(" + index + ", 1000);"),
                Arrays.asList("moveRightArrowToIndex(" + lastRightArrowIndex + ", 1000);"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);
        lastRightArrowIndex = index;
    }

    public void setToFinished(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("setToFinished(" + index + ");"),
                Arrays.asList("setToNotFinished(" + index + ");"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);

    }

    public void setPivotToFinished(int index) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("deselectPivotElement(" + index + ");", "setToFinished(" + index + ");"),
                Arrays.asList("setToNotFinished(" + index + ");", "selectPivotElement(" + index + ");"),
                FrontEndEvent.NORMAL
        );
        eventManager.addEvent(event);

    }

    public void setLeftArrowNumber(int nr) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("setLeftArrowNumber(" + nr + ");"),
                Arrays.asList("setLeftArrowNumber(" + lastLeftArrowNr + ");"),
                FrontEndEvent.NO_DELAY
        );
        eventManager.addEvent(event);
        lastLeftArrowNr = nr;
    }

    public void setRightArrowNumber(int nr) {
        FrontEndEvent event = new FrontEndEvent(
                Arrays.asList("setRightArrowNumber(" + nr + ");"),
                Arrays.asList("setRightArrowNumber(" + lastRightArrowNr + ");"),
                FrontEndEvent.NO_DELAY
        );
        eventManager.addEvent(event);
        lastRightArrowNr = nr;
    }

    private void setLevelHeight(int height) {
        executeScript("setLevelHeight(" + height + ");");
    }
}
