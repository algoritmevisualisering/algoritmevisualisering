package algorithms.QuickSortAlgorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This class is created by kristianrosland on 06.09.2016.
 */
public class QuickSortAlgorithm {

    private Integer [] array;
    private QuickSortView view;


    public QuickSortAlgorithm(QuickSortView view) {
        this.view = view;
    }

    public void sort() {
        if (array == null) return;
        sort(0, array.length - 1); //Sort a from 0 to (length - 1)
    }

    // quicksort the sub-array from a[lo] to a[hi]
    private void sort(int lo, int hi) {
        if (hi <= lo) {
            view.setToFinished(lo);
            return;
        }
        int j = partition(this.array, lo, hi);

        view.lowerElements(lo, j-1);
        sort(lo, j - 1);
        view.liftElements(lo, j-1);
        view.lowerElements(j+1, hi);
        sort(j + 1, hi);
        view.liftElements(j+1, hi);
    }

    /**
     *  Partition the sub-array from 'lo' to 'hi' so that a[lo..j-1] <= a[j] <= a[j+1..hi]
     *  @return The pivot index j
     */
    private int partition(Integer [] a, int lo, int hi) {
        int i = lo;
        view.setIIndex(i);
        int j = hi + 1;
        view.setJIndex(j);
        Integer v = a[lo];
        view.setPivotElement(lo);
        view.setLeftArrowNumber(v);
        view.setRightArrowNumber(v);
        view.setLeftAndRightArrow(i+1, j-1);
        view.showArrows();
        while (true) {

            // find item on lo to swap
            while (less(a[++i], v)) {
                view.moveLeftArrow(i);
                view.setIIndex(i);
                if (i == hi) break;
            }
            view.moveLeftArrow(i); // because of the last ++i

            // find item on hi to swap
            while (less(v, a[--j])) {
                view.moveRightArrow(j);
                view.setJIndex(j);
                if (j == lo) break;      // redundant since a[lo] acts as sentinel
            }
            view.moveRightArrow(j); // because of the last --i

            // check if pointers cross
            if (i >= j) break;

            swap(a, i, j);
            view.swapElements(i, j);
        }

        // put partitioning item v at a[j]
        swap(a, lo, j);
        view.swapElements(lo, j);
        view.hideArrows();
        view.setPivotToFinished(j);

        // now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
        return j;
    }

    /***************************************************************************
     * Helper sorting functions.
     ***************************************************************************/

    // is v < w ?
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    // exchange a[i] and a[j]
    private static void swap(Integer[] a, int i, int j) {
        Integer swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    /**
     * Shuffle an array
     * @param arr Array to be shuffled
     */
    private static void shuffle(Integer[] arr) {
        List<Integer> list = Arrays.asList(arr);
        Collections.shuffle(list);
        for (int i = 0; i < list.size(); i++) arr[i] = list.get(0);
    }

    public Integer [] getArray() { return this.array; }
    public int arrayLength() { return this.array.length; }
    public void setArray(Integer [] arr) { this.array = arr; }

}
