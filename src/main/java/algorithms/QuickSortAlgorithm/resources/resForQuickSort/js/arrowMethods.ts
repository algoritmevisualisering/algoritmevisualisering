///<reference path="initArray.ts"/>
///<reference path="methods.ts"/>

var MARGIN_TO_ARROW = 100;
var ARROW_LEVEL = 0;

function setupArrows() {
    setArrowLevel(0, 0)
}

function setArrowLevel(level:number, animTime:number) {
    var top:number = MARGIN_TO_ARROW + LEVEL_HEIGHT*level;
    $("#leftArrow, #rightArrow").each(function() {
        $(this).animate({ top : top + "px" }, animTime);
    });
}

function resetArrows() {
    ARROW_LEVEL = 0;
    hideLeftArrow();
    hideRightArrow()
}

function liftArrows(animTime:number) {
    setArrowLevel(--ARROW_LEVEL, animTime);
}

function lowerArrows(animTime:number) {
    setArrowLevel(++ARROW_LEVEL, animTime);
}

function hideLeftArrow() {
    var $elem = $("#leftArrow");
    if ($elem.length == 0) { return; }

    $elem.addClass("hidden");
}

function hideRightArrow() {
    var $elem = $("#rightArrow");
    if ($elem.length == 0) { return; }

    $elem.addClass("hidden");
}

function showLeftArrow() {
    $("#leftArrow").removeClass("hidden");
}

function showRightArrow() {
    $("#rightArrow").removeClass("hidden");
}

function moveLeftArrowToIndex(i:number, animationTime:number) {
    $("#leftArrow").animate({left : (85*i-5) + "px" }, animationTime);
}

function moveRightArrowToIndex(i:number, animationTime:number) {
    $("#rightArrow").animate({left : (85*i-5) + "px" }, animationTime);
}

function setLeftArrowNumber(nr:number) {
    $("div#leftArrow div.arrowText").html("while<br>&lt; " + nr);
}

function setRightArrowNumber(nr:number) {
    $("div#rightArrow div.arrowText").html("while<br>&gt; " + nr);
}


