package algorithms.SimpleSorting;

import java.util.Stack;

/**
 * Created by kristianrosland on 14.04.2016.
 */
public class StateController {

    private Stack<int[]> previousStates, nextStates;
    private final Controller controller;

    public StateController(Controller controller) {
        previousStates = new Stack();
        nextStates = new Stack();
        this.controller = controller;
    }

    /**
     * Method for stepping back (if possible)
     * Also saves the current state to nextStates to be able to step forward again
     */
    public boolean stepBack() {
        if (previousStates.isEmpty())
            return false;

        saveStateToNextStates();
        int[] previousState = previousStates.pop();
        setState(previousState);
        return true;
    }


    /**
     * Method for stepping forward (if possible)
     * Also save the current state to previousStates to be able to step back again
     */
    public boolean stepForward() {
        if (nextStates.isEmpty())
            return false;

        saveStateToPreviousStates();
        int[] nextState = nextStates.pop();
        setState(nextState);
        return true;
    }

    /**
     * Method for saving the current state. Sent before every unionFind()-call
     */
    public void saveState() {
        nextStates.removeAllElements();
        saveStateToPreviousStates();
    }


    /** Private help methods to take care of states **/
    private void saveStateToPreviousStates() {
        int[] state = getState();
        previousStates.push(state);
    }

    private void saveStateToNextStates() {
        int[] state = getState();
        nextStates.push(state);
    }

    private int[] getState() {
        return controller.getArrayClone();
    }

    /**
     * Set the current state of the backend and frontend to parameter state
     * @param state
     */
    private void setState(int[] state) {
        controller.setArray(state);
    }
}
