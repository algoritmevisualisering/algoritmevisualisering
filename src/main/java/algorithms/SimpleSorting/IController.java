package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 17.06.2016.
 */
public interface IController {

    public final static int J_COLOR = 0;
    public final static int KEY_COLOR = 1;
    public final static int I_COLOR = 2;

    /**
     * Switches two elements in an Array
     * @param indexA The first element
     * @param indexB The second element
     */
    void switchArrayElements(int indexA, int indexB);

    /**
     * Starting the algorithm
     */
    void startInsertionSort();

    /**
     * Setting the value in the Array
     * @param index The index of the value
     * @param value The value
     */
    void setValueInArray(int index, int value);

    /**
     * Set new array
     * @param array The new array to set
     */
    void setArrayInBackend(int[] array);


    void setArrayInFrontend(int[] array);

    /**
     * Stores a value from the array to be used later
     * @param j Index of the value being stored
     */
    void storePermValue(int j);

    void releasePermValue(int j);

    /**
     * Asks the backend to set a random array
     */
    void setRandomArray();

    void setAlmostSortedArray();

    void setSortedArray();

    void setJElement(int j, boolean active);

    void setElementBeingComparedTo(int i, boolean active);

    void startShellSort();

    void moveArrayElementToIndex(int fromIndex, int toIndex);

    /**
     *  This method is used when moving the stored (green) index to its correct place.
     *  We cannot use the normal moveArrayElementToIndex() because we give it params (i+1, i+1),
     *  which would corrupt the backwards-stepping.
     *
     *  @param jIndex The current J-index of the algorithm
     */
    void moveArrayElementToIndexFromSpecifiedJIndex(int fromIndex, int toIndex, int jIndex);

    void setHeaderText(String name);

    void setKValue(int k);

    void setKLeftAndRight(int i, int k);

    void unhideK();

    void hideK();

    void setInvertedSortedArray();
}
