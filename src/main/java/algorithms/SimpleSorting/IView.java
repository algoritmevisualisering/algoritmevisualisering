package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 17.06.2016.
 */
public interface IView {

    void switchArrayElements(int indexA, int indexB);

    void moveArrayElementToIndex(int fromIndex, int toIndex);

    /** @see IController for documentation **/
    void moveArrayElementToIndexFromSpecifiedJIndex(int fromIndex, int toIndex, int jIndex);

    void startInsertionSort();

    void setValueInArray(int index, int value);

    void setColorInArrayElement(int index, int color, boolean colorOn);

    void setArray(int[] array);

    void storePermValue(int j);

    void releasePermValue(int j);

    void lockControllers();

    void enableButtons();

    void setHeaderText(String name);

    void setKValue(int k);

    void setKLeftAndRight(int i, int k);

    void unhideK();

    void hideK();
}
