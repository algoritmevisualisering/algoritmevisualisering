package algorithms.SimpleSorting;

import javafx.application.Platform;
import javafx.scene.web.WebEngine;

import java.util.List;

/**
 * Created by KnutAnders on 17.07.2016.
 */
public class FrontEndEvent {


    private final List<String> execute, reverse;
    private final double type;

    public static final double NO_DELAY = 0, SHORT = 0.5, NORMAL = 1, LONG = 2.5, EXTRA_LONG = 4.0;

    public double getType() {
        return type;
    }

    public FrontEndEvent(List<String> execute, List<String> reverse, double type) {
        this.execute = execute;
        this.reverse = reverse;
        this.type = type;
    }

    private void runEvent(WebEngine engine, List<String> list) {
        if (list.isEmpty()) return;

        Platform.runLater(() -> {
            for (String s : list) {
                engine.executeScript(s);
            }
        });
    }

    public void execute(WebEngine engine) {
        runEvent(engine, execute);
    }

    public void reverse(WebEngine engine) {
        runEvent(engine, reverse);
    }

    public List<String> getExecute() {
        return execute;
    }

    public List<String> getReverse() {
        return reverse;
    }

    public boolean isColorEvent() {
        return execute.get(0).startsWith("setColor");
    }

    public boolean hasDelay() {
        return type != NO_DELAY;
    }
}
