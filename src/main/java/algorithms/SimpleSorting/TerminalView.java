package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 17.06.2016.
 */
public class TerminalView implements IView {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_PURPLE = "\u001B[35m";

    private IController controller;
    private int[] array = {5, 4, 3, 2, 1, 0};
    private int permIndex = -1;
    private int permValue = -1;

    public TerminalView() {
        this.controller = new Controller(new InsertionSort(), this);
    }

    public static void main(String[] args) {
        TerminalView view = new TerminalView();
        view.setArray(view.getArray());
        view.startInsertionSort();
    }

    private void printArray() {
        for (int i=0; i<array.length; i++) {
            if (i != permIndex)
                System.out.print(array[i]);
            else
                System.out.print(ANSI_RED + permValue + ANSI_RESET);
            System.out.print("   ");
        }
        System.out.println();
    }

    @Override
    public void switchArrayElements(int indexA, int indexB) {
        if (indexA < 0 || indexA >= array.length || indexB < 0 || indexB >= array.length)
            throw new IllegalArgumentException("The array does not have this index: indexA="+indexA+", indexB="+indexB);
        if (permIndex == indexA)
            permIndex = indexB;
        else if (permIndex == indexB)
            permIndex = indexA;
        int perm = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = perm;
        printArray();
    }

    @Override
    public void moveArrayElementToIndex(int fromIndex, int toIndex) {
        // TODO implement?
    }

    @Override
    public void moveArrayElementToIndexFromSpecifiedJIndex(int fromIndex, int toIndex, int jIndex) {

    }

    @Override
    public void startInsertionSort() {
        printArray();
        controller.setArrayInBackend(array);
        controller.startInsertionSort();
        printArray();
    }

    @Override
    public void setValueInArray(int index, int value) {
        if (index < 0 || index >= array.length)
            throw new IllegalArgumentException("Index out of bounds");
        array[index] = value;
    }

    @Override
    public void setColorInArrayElement(int index, int color, boolean colorOn) {

    }

    @Override
    public void setArray(int[] array) {
        this.array = array;
        controller.setArrayInBackend(array);
    }

    public int[] getArray() {
        return array;
    }

    @Override
    public void storePermValue(int j) {
        permIndex = j;
        permValue = array[j];
        System.out.println(System.lineSeparator() + "Next Value: " + ANSI_PURPLE + array[j] + ANSI_RESET + System.lineSeparator());
        printArray();
    }

    @Override
    public void releasePermValue(int j) {
        permIndex = -1;
        permValue = -1;
        printArray();
    }

    @Override
    public void lockControllers() {}

    @Override
    public void enableButtons() {}

    @Override
    public void setHeaderText(String name) {
        System.out.println("Name: " + name);
    }

    @Override
    public void setKValue(int k) {

    }

    @Override
    public void setKLeftAndRight(int i, int k) {

    }

    @Override
    public void unhideK() {

    }

    @Override
    public void hideK() {

    }
}
