package algorithms.SimpleSorting;

import algorithms.IntArrayGenerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import static java.lang.Thread.sleep;

/**
 * Class for sorting int array using insertion insertionSort algorithm
 *
 * Created by KnutAnders
 */
public class InsertionSort implements SimpleSorting {

    private int [] array = new int[15];
    private IController controller;
    private Random rand;
    private IntArrayGenerator gen = new IntArrayGenerator();

    public InsertionSort() {
        rand = new Random();
    }

    @Override
    public void insertionSort() {
        controller.setHeaderText("Insertion Sort");
        controller.hideK();

        int j; // Elements sorted, starting on second position
        int key; // Current element
        int i; // Index moving backwards with key

        for (j = 1; j<array.length; j++) {
            controller.setJElement(j, true);
            controller.setElementBeingComparedTo(j-1, true);
            key = array[j];
            controller.storePermValue(j);


            // Move pointer backwards until it finds correct position. Moves every element it passes one right.
            for (i = j-1; i >= 0 && array[i]>key; i--) {
                controller.setElementBeingComparedTo(i, true);
                controller.moveArrayElementToIndex(i, i+1);

                array[i + 1] = array[i];

                controller.setElementBeingComparedTo(i+1, false);
            }
            controller.moveArrayElementToIndexFromSpecifiedJIndex(i+1, i+1, j);
            array[i + 1] = key; // i+1 because for i will decrease one extra in the for loop

            controller.releasePermValue(i+1);
            controller.setJElement(i+1, false);
            controller.setElementBeingComparedTo(j-1, false);
        }
    }

    @Override
    public void shellSort() {
        controller.setHeaderText("Shell Sort");

        // Setup K
        int k = (int) Math.floor(array.length/2); // Gap
        controller.setKValue(k);
        controller.setKLeftAndRight(0, k);
        controller.unhideK();

        while (k != 0) {
            for (int j = k; j<array.length; j++) {
                controller.setKLeftAndRight(j-k, j);
                controller.setJElement(j, true);
                controller.setElementBeingComparedTo(j-k, true);

                int key = array[j];
                controller.storePermValue(j);

                int i;
                for (i = j-k; i >= 0 && array[i] > key; i = i-k) {
                    controller.setKLeftAndRight(i, i+k);
                    controller.setElementBeingComparedTo(i, true);
                    array[i+k] = array[i];
                    controller.switchArrayElements(i + k, i);
                    controller.setElementBeingComparedTo(i+k, false);
                }
                array[i+k] = key;
                controller.releasePermValue(i + k);
                controller.setElementBeingComparedTo(j-k, false);
                controller.setJElement(i + k, false);

            }
            k = (int) Math.floor(k/2);
            controller.setKValue(k);
        }

        controller.hideK();
    }

    public void delay() {
        try {
            sleep(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setArray(int[] array) {
        this.array = array;
    }

    @Override
    public int[] getArray() {
        return array;
    }

    @Override
    public void setController(IController controller) {
        this.controller = controller;
    }

    @Override
    public void setRandomArray() {
        for (int i=0; i<array.length; i++)
            array[i] = rand.nextInt(100);
        controller.setArrayInFrontend(array);
    }

    @Override
    public void setAlmostSortedArray() {
        array = gen.integerToIntArray(gen.getIntegerArray(array.length, IntArrayGenerator.ARRAY_TYPE.ALMOST_SORTED));
        controller.setArrayInFrontend(array);
    }

    @Override
    public void setSortedArray() {
        for(int i=0; i<array.length; i++) { array[i] = rand.nextInt(100); }
        Arrays.sort(array);
        controller.setArrayInFrontend(array);
    }

    public void setInvertedSortedArray() {
        Integer[] midlArray = new Integer[array.length];
        for(int i=0; i<midlArray.length; i++) { midlArray[i] = rand.nextInt(100); }
        Arrays.sort(midlArray, Collections.reverseOrder());
        for (int i=0; i<midlArray.length; i++)
            array[i] = midlArray[i];
        controller.setArrayInFrontend(array);
    }
}
