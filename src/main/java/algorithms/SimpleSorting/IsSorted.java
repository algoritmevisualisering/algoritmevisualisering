package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 16.07.2016.
 */
public class IsSorted {

    public static boolean isSorted(int[] array) {
        for (int i=1; i<array.length; i++) {
            if (array[i] < array[i-1])
                return false;
        }
        return true;
    }

    public static boolean isSorted(Integer[] array) {
        int[] newArray = new int[array.length];
        for (int i=0; i<array.length; i++)
            newArray[i] = array[i];
        return isSorted(newArray);
    }

}
