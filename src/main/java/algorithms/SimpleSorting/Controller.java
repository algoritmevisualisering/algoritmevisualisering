package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 17.06.2016.
 */
public class Controller implements IController {

    private final IView view;
    private SimpleSorting simpleSorting;

    public Controller(SimpleSorting simpleSorting, IView view) {
        this.view = view;
        this.simpleSorting = simpleSorting;
        simpleSorting.setController(this);
    }

    public void setSimpleSorting(SimpleSorting ss) {
        this.simpleSorting = ss;
    }

    @Override
    public void switchArrayElements(int indexA, int indexB) {
        view.switchArrayElements(indexA, indexB);
    }

    @Override
    public void moveArrayElementToIndex(int fromIndex, int toIndex) {
        view.moveArrayElementToIndex(fromIndex, toIndex);
    }

    @Override
    public void moveArrayElementToIndexFromSpecifiedJIndex(int fromIndex, int toIndex, int jIndex) {
        view.moveArrayElementToIndexFromSpecifiedJIndex(fromIndex, toIndex, jIndex);
    }

    @Override
    public void setHeaderText(String name) {
        view.setHeaderText(name);
    }

    @Override
    public void setKValue(int k) {
        view.setKValue(k);
    }

    @Override
    public void setKLeftAndRight(int i, int k) {
        view.setKLeftAndRight(i, k);
    }

    @Override
    public void unhideK() {
        view.unhideK();
    }

    @Override
    public void hideK() {
        view.hideK();
    }

    @Override
    public void setInvertedSortedArray() {
        simpleSorting.setInvertedSortedArray();
    }

    @Override
    public void startInsertionSort() {
        simpleSorting.insertionSort();
    }

    @Override
    public void startShellSort() {
        simpleSorting.shellSort();
    }

    @Override
    public void setValueInArray(int index, int value) {

    }

    private void setColorInArrayElement(int index, int color, boolean colorOn) {
        view.setColorInArrayElement(index, color, colorOn);
    }

    @Override
    public void setArrayInBackend(int[] array) {
        simpleSorting.setArray(array);
    }

    @Override
    public void setArrayInFrontend(int[] array) {
        view.setArray(array);
    }

    @Override
    public void storePermValue(int j) {
        view.storePermValue(j);
    }

    @Override
    public void releasePermValue(int j) {
        view.releasePermValue(j);
    }

    @Override
    public void setRandomArray() {
        simpleSorting.setRandomArray();
    }

    @Override
    public void setAlmostSortedArray() {
        simpleSorting.setAlmostSortedArray();
    }

    @Override
    public void setSortedArray() {
        simpleSorting.setSortedArray();
    }

    @Override
    public void setJElement(int j, boolean active) {
        setColorInArrayElement(j, IController.J_COLOR, active);
    }

    @Override
    public void setElementBeingComparedTo(int i, boolean active) {
        setColorInArrayElement(i, IController.I_COLOR, active);
    }

    public int[] getArrayClone() {
        return simpleSorting.getArray();
    }

    public void setArray(int[] array) {
        simpleSorting.setArray(array);
    }

}
