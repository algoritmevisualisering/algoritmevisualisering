package algorithms.SimpleSorting;

import javafx.scene.web.WebEngine;

import java.util.*;
import java.util.concurrent.*;

/**
 * Manages FrontEnd events.
 * Can constantly play through all steps, or step forward and backward linearly
 * If stepped backward, and new event is added, the next-queue is cleared
 */
public class EventManager implements Runnable{

    private BlockingDeque<FrontEndEvent> nextEvents; // Thread safe
    private BlockingDeque<FrontEndEvent> previousEvents; // Thread safe

    private int delay = 750;
    private boolean play = true, past = false;
    private WebEngine engine;

    public EventManager(WebEngine engine) {
        this.engine = engine;
        nextEvents = new LinkedBlockingDeque<>();
        previousEvents = new LinkedBlockingDeque<>();
        new Thread(this).start();
    }

    // TODO: Called on every addEvent() ?
    public void play() {
        play = true;
        engine.executeScript("setPauseButtonText(0);");
        engine.executeScript("togglePauseIcon(1);");


        synchronized (this) {
            notify();
        }
    }

    public void removePauseIcon() {
        engine.executeScript("togglePauseIcon(1);");
    }

    public void insertPauseIcon() {
        engine.executeScript("togglePauseIcon(0);");
    }

    public void pause() {
        play = false;
        engine.executeScript("setPauseButtonText(1);");
        engine.executeScript("togglePauseIcon(0);");
    }

    public void clear() {
        nextEvents.clear();
        previousEvents.clear();
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * Executes next event if thread is paused
     * @return True if next event was executed
     */
    public boolean nextEvent() {
        if (!play && nextEvents.peek() != null) {
            FrontEndEvent e = nextEvents.poll();
            e.execute(engine);
            previousEvents.add(e);
            if (e.isColorEvent() || !e.hasDelay()) { return nextEvent(); }
            return true;
        }
        else
            return false;
    }

    public boolean oneStepBack() {
        if (!play && !previousEvents.isEmpty()) {
            FrontEndEvent event = previousEvents.removeLast();
            event.reverse(engine);
            nextEvents.addFirst(event);
            if (event.isColorEvent() || !event.hasDelay()) { return oneStepBack(); } //To avoid user having too press to many times
            return true;
        }
        return false;
    }


    public void addEvent(FrontEndEvent newEvent) {
        if (past) {
            nextEvents.clear(); // To prevent multiple futures
            past = false;
        }
        nextEvents.addLast(newEvent);
        if (play)
            play();
    }

    /**
     * The thread where manager pushes events to FrontEnd
     */
    @Override
    public void run() {
        while (true) {
            try {
                // Wait for next event in queue -> execute event
                if (nextEvents.peek() != null) {
                    FrontEndEvent e = nextEvents.poll();
                    e.execute(engine);
                    previousEvents.add(e);
                    Thread.sleep((int)(delay * e.getType()));
                }
                else
                    Thread.sleep(10);
                if (!play)
                    synchronized (this) {
                        wait();
                    }
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void togglePause() {
        togglePause(null, false);
    }

    /**
     *  This method is only used with callback in InsertionSort. QuickSort uses togglePause()
     *   @param restart If the array has been reset
     */
    public void togglePause(Runnable callBackMethod, boolean restart) {
        if (play) {
            pause();
        }
        else {
            play();
            if (restart) {
                callBackMethod.run();
            }
        }
    }

    public void unpause() {
        play();
    }

    public boolean isEmpty() {
        return nextEvents.isEmpty();
    }
}
