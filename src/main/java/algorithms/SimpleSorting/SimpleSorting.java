package algorithms.SimpleSorting;

/**
 * Created by KnutAnders on 09.06.2016.
 */
public interface SimpleSorting {

    void insertionSort();

    void shellSort();

    void setArray(int[] array);

    int[] getArray();

    void setController(IController controller);

    void setRandomArray();

    void setAlmostSortedArray();

    void setSortedArray();

    void setInvertedSortedArray();
}
