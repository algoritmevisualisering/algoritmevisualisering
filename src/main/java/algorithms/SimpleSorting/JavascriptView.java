package algorithms.SimpleSorting;

import algorithms.IntArrayGenerator;
import algorithms.webViewUtils.JavaLog;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static algorithms.webViewUtils.Java2JavascriptUtils.connectBackendObject;

/**
 * Created by KnutAnders on 17.06.2016.
 */
public class JavascriptView extends Application implements IView {

    private WebView webView;
    private IController controller;
    private String[] colors = {"#7FFF00", "not used", "#FFB366"};
    private boolean arrayIsReset = false;
    private int k = 0, kLeft = 0, kRight = 0;
    private String currentAlgorithmName = "Insertion sort";

    IntArrayGenerator arrayGenerator = new IntArrayGenerator();

    EventManager eventManager;

    public static void main(String[] args) {
        System.setProperty("prism.lcdtext", "false"); // enhance fonts
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        setController(new Controller(new InsertionSort(), this));
        createWindow(primaryStage, "/resForSimpleSort/index.html");
    }

    private String serializeArray(int[] array) {
        String returnString = "";
        for (int i : array)
            returnString = returnString.concat(i + "|");
        return returnString.substring(0, returnString.length()-1);
    }

    private void createWindow(Stage primaryStage, String path) {
        webView = new WebView();
        eventManager = new EventManager(webView.getEngine());
        double screenHeight = 500, screenWidth = Screen.getPrimary().getVisualBounds().getWidth();

        // Use connectBackEndObject to connect an object to a js variable
        connectBackendObject(webView.getEngine(), "array", serializeArray(arrayGenerator.getArray(15)));
        connectBackendObject(webView.getEngine(), "javaLog", new JavaLog());
        connectBackendObject(webView.getEngine(), "javaBinder", this);

        webView.getEngine().setOnAlert((event) -> System.out.println("WebView: " + event.getData()));

        primaryStage.setOnCloseRequest((i) -> System.exit(0));

        webView.setPrefSize(screenWidth, screenHeight);
        webView.getEngine().load(getClass().getResource(path).toExternalForm());

        webView.setContextMenuEnabled(false);

        primaryStage.setScene(new Scene(webView));
        primaryStage.setTitle("Sorting");
        primaryStage.show();
    }

    @Override
    public void switchArrayElements(int indexA, int indexB) {
        List<String> forwardSteps = new ArrayList<>();
        forwardSteps.add("setPosition(" + indexA + ", " + indexB * 70 + ", 0)");
        forwardSteps.add("setPosition(" + indexB + ", " + indexA * 70 + ", 0)");
        forwardSteps.add("swapId(" + indexA + ", " + indexB + ")");

        List<String> backwardSteps = new ArrayList<>();
        backwardSteps.add("setPosition(" + indexB + ", " + indexB * 70 + ", 0)");
        backwardSteps.add("setPosition(" + indexA + ", " + indexA * 70 + ", 0)");
        backwardSteps.add("swapId(" + indexB + ", " + indexA + ")");

        eventManager.addEvent(new FrontEndEvent(forwardSteps, forwardSteps, FrontEndEvent.LONG));
    }

    @Override
    public void moveArrayElementToIndex(int fromIndex, int toIndex) {
        List<String> forwardSteps = new ArrayList<>();
        forwardSteps.add("setPosition(" + fromIndex + ", " + toIndex * 70 + ", 0)");
        forwardSteps.add("swapId(" + fromIndex + ", " + toIndex + ")");

        List<String> backwardSteps = new ArrayList<>();
        backwardSteps.add("swapId(" + toIndex + ", " + fromIndex + ")");
        backwardSteps.add("setPosition(" + fromIndex + ", " + fromIndex * 70 + ", 0)");

        eventManager.addEvent(new FrontEndEvent(forwardSteps, backwardSteps, FrontEndEvent.NORMAL));
    }

    @Override
    public void moveArrayElementToIndexFromSpecifiedJIndex(int fromIndex, int toIndex, int jIndex) {
        List<String> forwardSteps = new ArrayList<>();
        forwardSteps.add("setPosition(" + fromIndex + ", " + toIndex * 70 + ", 0)");

        //We use jIndex for the backward-step, because this is where it originally came from
        List<String> backwardSteps = new ArrayList<>();
        backwardSteps.add("setPosition(" + fromIndex + ", " + jIndex * 70 + ", 0)");

        eventManager.addEvent(new FrontEndEvent(forwardSteps, backwardSteps, FrontEndEvent.NORMAL));
    }

    @Override
    public void startInsertionSort() {
        if (!arrayIsReset) {
            setRandomArray();
            this.executeScript("setPauseButtonText(0);");
        }
        arrayIsReset = false;
        controller.startInsertionSort();
    }

    public void startShellSort() {
        if (!arrayIsReset) {
            setRandomArray();
            this.executeScript("setPauseButtonText(0);");
        }
        arrayIsReset = false;
        controller.startShellSort();
    }

    @Override
    public void setValueInArray(int index, int value) {

    }

    @Override
    public void setColorInArrayElement(int index, int color, boolean colorOn) {
        eventManager.addEvent(new FrontEndEvent(
                Arrays.asList("setColor(" + index + ", '" + colors[color] + "', " + colorOn + ")"),
                Arrays.asList("setColor(" + index + ", '" + colors[color] + "', " + !colorOn + ")"),
                FrontEndEvent.NO_DELAY) //NO-DELAY to avoid 'lag' when selecting
        );
    }

    @Override
    public void setArray(int[] array) {
        executeScript("k.hide();");
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("setArray('" + serializeArray(array) + "')"), null, FrontEndEvent.LONG));
    }

    public void setRandomArray() {
        this.executeScript("setPauseButtonText(1);");
        eventManager.clear();
        unpause();
        controller.setRandomArray();
        arrayIsReset = true;
    }

    public void setAlmostSortedArray() {
        this.executeScript("setPauseButtonText(1);");
        eventManager.clear();
        unpause();
        controller.setAlmostSortedArray();
        arrayIsReset = true;
    }

    public void setSortedArray() {
        this.executeScript("setPauseButtonText(1);");
        eventManager.clear();
        unpause();
        controller.setSortedArray();
        arrayIsReset = true;
    }

    public void setInvertedSortedArray() {
        this.executeScript("setPauseButtonText(1);");
        eventManager.clear();
        unpause();
        controller.setInvertedSortedArray();
        arrayIsReset = true;
    }

    @Override
    public void storePermValue(int index) {
        eventManager.addEvent(new FrontEndEvent(
                Arrays.asList("storePermValue(" + index + ")"),
                Arrays.asList("releasePermValue(" + index + ")"),
                FrontEndEvent.NORMAL)
        );
    }

    @Override
    public void releasePermValue(int index) {
        eventManager.addEvent(new FrontEndEvent(
                Arrays.asList("releasePermValue(" + index + ")"),
                Arrays.asList("storePermValue(" + index + ")"),
                FrontEndEvent.NORMAL)
        );
    }

    @Override
    public void lockControllers() {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("disableControllers()"), null, FrontEndEvent.NO_DELAY));
        // executeScript("disableControllers()");
    }

    @Override
    public void enableButtons() {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("enableControllers()"), null, FrontEndEvent.NO_DELAY));
        // executeScript("enableControllers()");
    }

    @Override
    public void setHeaderText(String name) {
        this.executeScript(String.format("setHeaderText(\'%s\')", name));
        currentAlgorithmName = name;
    }

    public void setController(Controller c) {
        controller = c;
    }

    private void executeScript(String script) {
        Platform.runLater(() -> webView.getEngine().executeScript(script));
    }

    public void togglePause() {
        if (currentAlgorithmName.toLowerCase().startsWith("insertion")) {
            eventManager.togglePause(() -> startInsertionSort(), arrayIsReset);
        } else {
            eventManager.togglePause(() -> startShellSort(), arrayIsReset);
        }
    }

    private void unpause() {
        eventManager.unpause();
    }

    public void forward() {
        eventManager.nextEvent();
    }

    public void backward() {
        eventManager.oneStepBack();
    }

    public void setKValue(int value) {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("k.setValue(" + value + ");"),
                Arrays.asList("k.setValue(" + k + ");"), FrontEndEvent.NORMAL));
        k = value;
    }

    public void setKLeftAndRight(int left, int right) {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("k.setLeftAndRight(" + left + ", " + right + ");"),
                Arrays.asList("k.setLeftAndRight(" + kLeft + ", " + kRight + ");"), FrontEndEvent.NORMAL));
        kLeft = left;
        kRight = right;
    }

    public void hideK() {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("k.hide();"), Arrays.asList("k.unhide();"), FrontEndEvent.NORMAL));
    }

    public void unhideK() {
        eventManager.addEvent(new FrontEndEvent(Arrays.asList("k.unhide();"), Arrays.asList("k.hide();"), FrontEndEvent.NORMAL));
    }
}
