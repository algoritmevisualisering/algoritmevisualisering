package algorithms.unionFind;

/**
 * Created by kristianrosland on 18.04.2016.
 */
public class WeightedUnionPathCompression extends QuickUnionPathCompression {
    private int[] treeSize; // number of elements in tree rooted at index
    private boolean paused = false;

    public WeightedUnionPathCompression(int arrSize) {
        super(arrSize);
        treeSize = new int[arrSize];
        for (int i = 0; i < arrSize; i++)
            treeSize[i] = 1;

        paused = false;
    }

    @Override
    public void union(int aIndex, int bIndex) {
        int aRoot = simpleFind(aIndex, "green");
        controller.displaySize(aRoot, treeSize[aRoot]);

        int bRoot = simpleFind(bIndex, "green");
        controller.displaySize(bRoot, treeSize[bRoot]);

        controller.saveState(getArray());

        //Only connectNodes if they were not already in the same tree
        if (aRoot != bRoot) {
            if (treeSize[bRoot] < treeSize[aRoot]) {
                controller.connectNodes(bRoot, aRoot);
                controller.setValueAtIndex(bRoot, aRoot);
                getArray()[bRoot] = aRoot;
                treeSize[aRoot] += treeSize[bRoot];
            } else {
                controller.connectNodes(aRoot, bRoot);
                controller.setValueAtIndex(aRoot, bRoot);
                getArray()[aRoot] = bRoot;
                treeSize[bRoot] += treeSize[aRoot];
            }
        }
        delay((getDelayTime()*2));

        removeHighlighting(aRoot);
        removeHighlighting(bRoot);
        controller.setSelectedIndex(aIndex, false);
        controller.setSelectedIndex(bIndex, false);
    }

    @Override
    public String getName() {
        return "WU w/ Path Compression";
    }

    @Override
    public void setArray(int [] array) {
        super.setArray(array);
        this.refreshSizeArray(array);
    }

    private void refreshSizeArray(int[] array) {
        treeSize = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int root = super.getRoot((array[i]));
            treeSize[root]++;
        }
    }
}
