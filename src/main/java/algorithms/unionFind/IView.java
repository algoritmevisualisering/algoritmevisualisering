package algorithms.unionFind;

import org.json.JSONArray;

/**
 * Created by knutandersstokke on 10.08.2016.
 */
public interface IView {
    void displayArray(int[] array);

    void selectIndex(int index, boolean b);

    void saveState(String relationshipsJSON, String backendArray);

    void setArrow(int index);

    void setValueAtIndex(int i, int bValue);

    void connectNodes(int child, int parent);

    void executeSaveMethodInJavaScript(int[] clonedBackendArray);

    void highlightNode(int index, String color);

    void removeHighlight(int node);

    void checkMark(int aIndex, int bIndex, boolean set);

    void redCross(int aIndex, int bIndex, boolean set);

    void displayNodeSize(int root, int size);

    void setState(JSONArray arrayObject, JSONArray array);

    void screenLock(boolean locked);

}
