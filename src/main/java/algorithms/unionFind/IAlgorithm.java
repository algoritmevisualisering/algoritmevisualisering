package algorithms.unionFind;

/**
 * Created by KnutAnders on 02.04.2016.
 */
public interface IAlgorithm {


    /**
     *  Union two elements on indexA and indexB
     * @param a index a
     * @param b index b
     */
    public void union(int a, int b);
    public int find(int a);
    public boolean connected(int a, int b);
    public String getName();

    /**
     *  Return the current state of the array
     * @return
     */
    public int [] getArray();

    /**
     *  Called when the pause-button (SPACE) is hit
     *  Sets the pause variable to !pause
     */
    public void invertPause();

    /**
     *  Set the controller for this algorithm
     * @param controller
     */
    public void setController(Controller controller);

    /**
     *  Set the current state of the algorithm to the given array
     * @param array
     */
    void setArray(int[] array);

    /**
     *  Does the same as connected but does not update the GUI
     */
    boolean connectedNoGUIUpdate(int a, int b);
}
