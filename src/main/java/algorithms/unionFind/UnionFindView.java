package algorithms.unionFind;

import algorithms.webViewUtils.JavaLog;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Constructor;

import static algorithms.webViewUtils.Java2JavascriptUtils.connectBackendObject;

/**
 * Main class for Union Find
 * @author KnutAnders
 */
public class UnionFindView extends Application implements IView {

    private final static int DEFAULT_ARRAY_LENGTH = 10;
    private final static String TITLE = "Union Find";

    // JavaFx's Browser
    private WebView webView;

    private Controller controller;
    private boolean javaScriptReady = false;

    private StateController stepper;

    private final static String[] listOfAlgorithms = new String[] {
            "QuickFind", "QuickUnion", "WeightedUnion", "QuickUnionPathCompression", "WeightedUnionPathCompression"
    };
    private int currentAlgorithm = 0;

    // The Main Method
    public static void main(String[] args){
        System.setProperty("prism.lcdtext", "false"); // enhance fonts
        launch(args);
    }

    // JavaFX-method. Called from launch() in main. Sets controller and creates browser window
    @Override
    public void start(Stage primaryStage) throws Exception {
        createWindow(primaryStage, "/resForUnionFind/index.html");
        controller = new Controller(new QuickFind(DEFAULT_ARRAY_LENGTH));
        controller.setGUI(this);
        controller.setup();
        controller.changeSpeed(20);

        stepper = new StateController(controller, this);

    }

    // Method for creating browser window and binding variable to JavaScript
    // NB! Objects created here will belong to another instance of Application!
    private void createWindow(Stage primaryStage, String path) {

        webView = new WebView();

        // Connecting java variables to js
        sendVariablesToJavaScript();

        // Exit program when exit-button is pressed
        primaryStage.setOnCloseRequest((i) -> System.exit(0));

        // Start height
        webView.setPrefSize(1000, 800);

        // Let browser load html page
        webView.getEngine().load(getClass().getResource(path).toExternalForm());

        webView.setContextMenuEnabled(false);

        // Show browser to user
        primaryStage.setScene(new Scene(webView));
        primaryStage.setTitle(TITLE);
        primaryStage.show();

    }

    /**
     * Sending variable to JavaScript / WebEngine : connectBackendObject(webView.getEngine(), variableName, variable);
     * Variable must also be alerted from JavaScript side, see bindJavaObjects.js
     */
    private void sendVariablesToJavaScript() {
        connectBackendObject(webView.getEngine(), "javaLog", new JavaLog()); // JavaScript can now use Sysout
        connectBackendObject(webView.getEngine(), "javaBinder", this); // JavaScript can now use public functions from this class
        connectBackendObject(webView.getEngine(), "arrayLength", DEFAULT_ARRAY_LENGTH);
    }

    @Override
    public void displayArray(int[] array) {
        JSONArray json = convertArrayToJSON(array);
        executeScript("displayArray('" + json.toString() + "');");
    }

    private JSONArray convertArrayToJSON(int[] array) {
        try {
            JSONArray jsonArray = new JSONArray(array);
            return jsonArray;
        } catch (JSONException e) {
            throw new IllegalArgumentException("Error: Not able to convert array to JSON object.");
        }
    }

    @Override
    public void selectIndex(int index, boolean b) {
        executeScript("selectIndex(" + index + ", " + b + ");");
    }

    @Override
    public void saveState(String twoDimRelationships, String backendArray) {
        //Make JSON-arrays and pass to stepper.saveState()
        try {
            JSONArray twoDimRelationshipsJSON = new JSONArray(twoDimRelationships);
            JSONArray arrJSON = new JSONArray(backendArray);
            stepper.saveState(twoDimRelationshipsJSON, arrJSON);
        }
        catch (JSONException e) { e.printStackTrace(); }
    }

    @Override
    public void setArrow(int index) {
        executeScript("setArrow(" + index + ");");
    }

    @Override
    public void setValueAtIndex(int i, int bValue) {
        executeScript("setValueAtIndex(" + i + ", " + bValue + ");");
    }

    @Override
    public void connectNodes(int child, int parent) {
        String params = String.format("%d, %d", child, parent);
        executeScript("connectNodes(" + params + ");");
    }

    @Override
    public void executeSaveMethodInJavaScript(int[] clonedBackendArray) {
        try {
            JSONArray arr = new JSONArray(clonedBackendArray);
            executeScript("saveState('" + arr.toString() + "');");
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void highlightNode(int index, String color) {
        executeScript("highlightNode(" + index + ", \"" + color + "\");");
    }

    @Override
    public void removeHighlight(int index) {
        executeScript("removeHighlight(" + index + ");");
    }

    @Override
    public void checkMark(int aIndex, int bIndex, boolean set) {
        executeScript("setCheckMark(" + set + ", " + aIndex + ", " + bIndex + ");");
    }

    @Override
    public void redCross(int aIndex, int bIndex, boolean set) {
        executeScript("setWrongMark(" + set + ", " + aIndex + ", " + bIndex + ");");
    }

    @Override
    public void displayNodeSize(int root, int size) {
        //Not implemented
    }

    @Override
    public void setState(JSONArray relationships, JSONArray backendArray) {
        //Call setState() method with execute script, params arrayJSON and relationshipsJSON
        String call = String.format("setState(\'%s\', \'%s\');", backendArray, relationships);
        executeScript(call);
    }

    public void stepBack(String twoDimRelationshipsJSON, String backendArrayJSON) {
        step("backward", twoDimRelationshipsJSON, backendArrayJSON);
    }

    public void stepForward(String twoDimRelationshipsJSON, String backendArrayJSON) {
        step("forward", twoDimRelationshipsJSON, backendArrayJSON);
    }

    private void step(String direction, String twoDimRelationshipJSON, String backendArrayJSON) {
        try {
            JSONArray relationships = new JSONArray(twoDimRelationshipJSON);
            JSONArray backendArray = new JSONArray(backendArrayJSON);
            if (direction.equals("forward"))
                stepper.stepForward(relationships, backendArray);
            else if (direction.equals("backward"))
                stepper.stepBack(relationships, backendArray);
        } catch (JSONException jsonEx) {
            System.out.println(jsonEx.getMessage());
        }
    }

    private void executeScript(String methodName) {
        if (!javaScriptReady) {
            new Thread(() ->  {
                try {
                    Thread.currentThread().sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                executeScript(methodName);
            }).start();
            return;
        }
        Platform.runLater(() -> webView.getEngine().executeScript(methodName));
    }

    public void javascriptReady() {
        javaScriptReady = true;
        executeScript("console.log = function(message)\n" +
                "{\n" +
                "    javaLog.log(message);\n" +
                "};");

    }

    public void nextAlgorithm() {
        incrementAlgorithmIndex();
        changeToCurrentAlgorithm();
    }

    public void resetAll() {
        try {
            Class algorithm = Class.forName("algorithms.unionFind."+listOfAlgorithms[currentAlgorithm]);
            Constructor<IAlgorithm> cons = algorithm.getConstructor(int.class);
            controller.setAlgorithm(cons.newInstance(DEFAULT_ARRAY_LENGTH));
        }
        catch (Exception e) { e.printStackTrace(); }

        int [] arr = {0,1,2,3,4,5,6,7,8,9};
        stepper = new StateController(controller, this);
        resetArray(arr);
        displayArray(arr);
    }

    public void changeToCurrentAlgorithm() {
        resetAll();

        // Update header: name of algorithm
        executeScript("setHeaderText(\""+ controller.getNameOfCurrentAlgorithm() + "\");");
    }

    private void resetArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            setValueAtIndex(i, i);
        }
    }

    private void incrementAlgorithmIndex() {
        if (currentAlgorithm < listOfAlgorithms.length-1)
            currentAlgorithm++;
        else
            currentAlgorithm = 0;
    }

    public void screenLock(boolean locked) {
        executeScript("screenLock(" + locked + ");");
    }

    // ** Executing algorithms from view ** //
    public void union(int indexA, int indexB) {
        controller.union(indexA, indexB);
    }

    public void connected(int indexA, int indexB) {
        controller.connected(indexA, indexB);
    }

    public void find(int index) {
        controller.find(index);
    }

    public void setSlow() {
        controller.changeSpeed(80);
    }

    public void setMedium() {
        controller.changeSpeed(50);
    }

    public void setFast() {
        controller.changeSpeed(20);
    }

    public void switchAlgorithm(String algorithm) {
        if (listOfAlgorithms[currentAlgorithm].equals(algorithm))
            return;

        for (int i=0; i<listOfAlgorithms.length; i++) {
            if (listOfAlgorithms[i].equals(algorithm)) {
                currentAlgorithm = i;
                break;
            }
        }

        changeToCurrentAlgorithm();
    }
}
