package algorithms.unionFind;

/**
 * Created by pokki on 02/04/16.
 *
 * Quick find, slow union
 *
 */
public class QuickFind implements IAlgorithm {

    private final long DELAY_TIME = 100;
    private int[] arr;
    private boolean pause;
    private Controller controller;
    private String name = "Quick Find";

    /**
     * Initializes an array of given length with values equal to each index
     *
     * @param size
     */
    public QuickFind(int size) {
        arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = i;
        }
        pause = false;
    }

    /**
     * Connects two components together
     *
     * @param aIndex
     * @param bIndex
     */
    public void union(int aIndex, int bIndex) {
        int aRoot = arr[aIndex];
        int bRoot = arr[bIndex];

        if (aRoot == bRoot) {
            delay(getDelayTime());
            controller.setSelectedIndex(aIndex, false);
            controller.setSelectedIndex(bIndex, false);
            return;
        }

        controller.saveState(arr);

        for (int i = 0; i < arr.length; i++) {
            //If the animation is paused
            while (pause) { delay((long)(getDelayTime()*0.2)); }

            //Point the arrow under element i
            controller.setArrow(i); delay((long)(getDelayTime()*0.5));
            if (arr[i] == aRoot) {
                controller.setValueAtIndex(i, bRoot);
                controller.connectNodes(i, bRoot);
                arr[i] = bRoot;
                delay((long)(getDelayTime()*0.5));
            }
        }

        controller.setArrow(-1);
        controller.setSelectedIndex(bIndex, false);
        controller.setSelectedIndex(aIndex, false);
    }

    /**
     * Checks if two indexes are in the same component
     * @return true if connected, else false
     */
    public boolean connected(int aIndex, int bIndex) {
        boolean connected = simpleFind(aIndex, "orange") == simpleFind(bIndex, "orange");

        if (connected) {
            controller.highlightNode(arr[aIndex], "green");
            controller.highlightNode(arr[bIndex], "green");
            controller.checkMark(aIndex, bIndex, true); //Set check mark for both nodes
        } else
            controller.redCross(aIndex, bIndex, true);


        delay(getDelayTime()*2);
        removeHighlightFromRoot(aIndex);
        removeHighlightFromRoot(bIndex);
        controller.checkMark(aIndex, bIndex, false);
        controller.redCross(aIndex, bIndex, false);

        controller.setSelectedIndex(aIndex, false);
        controller.setSelectedIndex(bIndex, false);

        return connected;
    }

    @Override
    public String getName() {
        return name;
    }

    private void removeHighlightFromRoot(int pIndex) {
        controller.removeHighlight(arr[pIndex]);
    }

    private void delay(long delayTime) {
        try {
            Thread.sleep(delayTime);
        } catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * Finds the component of given index
     *
     * @param pIndex index to check
     * @return the root of given index
     */
    public int find(int pIndex) {
        int root = simpleFind(pIndex, "green");

        delay(getDelayTime());
        controller.removeHighlight(root);
        controller.setSelectedIndex(pIndex, false);

        return root;
    }

    private int simpleFind(int pIndex, String color) {
        int root = arr[pIndex];

        //If this was not the root, highlight it red for 1 second
        if (pIndex != root) {
            controller.highlightNode(pIndex, "orange");
            delay(getDelayTime());
            controller.removeHighlight(pIndex);
        }

        //Highlight the root green for 1 second
        controller.highlightNode(root, color);

        return root;
    }

    public int[] getArray() {
        return arr;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public boolean isPause() {
        return pause;
    }

    public synchronized void invertPause() {
        System.out.println((!pause ? "Pausing" : "Starting"));
        pause = !pause;
    }

    @Override
    public void setArray(int [] array) {
        this.arr = array;
    }

    @Override
    public boolean connectedNoGUIUpdate(int a, int b) {
        return arr[a] == arr[b];
    }

    public long getDelayTime() {
        return DELAY_TIME + controller.getSpeed()*40;
    }
}
