package algorithms.unionFind;

/**
 * Created by pokki on 02/04/16.
 *
 * Quick unionFind, slow find
 */

public class QuickUnion implements IAlgorithm {

    protected final long DELAY_TIME = 100;
    private int[] arr;
    private boolean paused = false;
    String name = "Quick Union";
    Controller controller;

    /**
     * Initializes an array of given length with values equal to each index
     *
     * @param size
     */
    public QuickUnion(int size) {
        arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = i;
        }
    }

    /**
     * Connects two components together. Make root of A point to root of B.
     *
     * @param aIndex
     * @param bIndex
     */
    public void union(int aIndex, int bIndex) {
        int aRoot = simpleFind(aIndex, "green");
        int bRoot = simpleFind(bIndex, "green");

        controller.saveState(arr);

        //Only connectNodes if they were not already in the same tree
        if (aRoot != bRoot) {
            removeHighlighting(aRoot);
            controller.connectNodes(aRoot, bRoot);
            controller.setValueAtIndex(aRoot, bRoot);
            arr[aRoot] = bRoot;
        }
        delay(getDelayTime()*2);

        removeHighlighting(aRoot);
        removeHighlighting(bRoot);
        controller.setSelectedIndex(aIndex, false);
        controller.setSelectedIndex(bIndex, false);
    }

    /**
     * Checks if two indexes are in the same component
     * @return
     */
    public boolean connected(int aIndex, int bIndex) {
        int aRoot = simpleFind(aIndex, "orange"), bRoot = simpleFind(bIndex, "orange");
        boolean connected = (aRoot == bRoot);

        if (connected) {
            controller.highlightNode(aRoot, "green");
            controller.checkMark(aIndex, bIndex, true);
        }
        else
            controller.redCross(aIndex, bIndex, true);

        delay(getDelayTime()*2);
        removeHighlighting(aRoot);
        removeHighlighting(bRoot);

        controller.checkMark(aIndex, bIndex, false);
        controller.redCross(aIndex, bIndex, false);
        controller.setSelectedIndex(aIndex, false);
        controller.setSelectedIndex(bIndex, false);

        return connected;
    }

    /**
     * Finds the component (root) of given index
     *
     * @param pIndex
     * @return
     */
    public int find(int pIndex) {
        int root = simpleFind(pIndex, "green");

        delay(getDelayTime());
        removeHighlighting(root);
        controller.setSelectedIndex(pIndex, false);

        return root;
    }

    int simpleFind(int pIndex, String color) {
        int root = pIndex;
        removeHighlighting(root);
        while (root != arr[root]) {
            highlightSingleNode(root, "orange");
            delay(getDelayTime());
            removeHighlighting(root);
            root = arr[root];
        }
        highlightSingleNode(root, color);

        return root;
    }

    void removeHighlighting(int node) {
        controller.removeHighlight(node);
    }

    int getRoot(int pIndex) {
        int root = pIndex;

        while (root != arr[root])
            root = arr[root];

        return root;
    }

    @Override
    public int[] getArray() { return arr; }

    @Override
    public String getName() { return name; }

    @Override
    public void invertPause() {
        paused = !paused;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void setArray(int[] array) {
        this.arr = array;
    }

    @Override
    public boolean connectedNoGUIUpdate(int a, int b) {
        return getRoot(a) == getRoot(b);
    }

    /**
     *  Highlight a single node in the graphical
     *  This removes all other highlighting
     * @param nodeIndex
     */
    public void highlightSingleNode(int nodeIndex, String color) {
        controller.highlightNode(nodeIndex, color);
    }

    /**
     *  Sleep the current thread for delayTime milliseconds
     * @param delayTime
     */
    void delay(long delayTime) {
        try {
            Thread.sleep(delayTime);
        } catch (InterruptedException e ) {
            System.out.println("Thread: " + Thread.currentThread() + " interrupted.");
            e.printStackTrace();
        }
    }

    public long getDelayTime() {
        return DELAY_TIME + controller.getSpeed()*50;
    }

}
