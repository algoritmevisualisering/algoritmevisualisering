package algorithms.unionFind;

/**
 * Created by pokki on 06/04/16.
 */
public class WeightedUnion extends QuickUnion {
    private int[] treeSize; // number of elements in tree rooted at index
    private boolean paused = false;

    public WeightedUnion(int arrSize) {
        super(arrSize);
        treeSize = new int[arrSize];
        for (int i = 0; i < arrSize; i++)
            treeSize[i] = 1;

        paused = false;
    }

    public void union(int aIndex, int bIndex) {
        int aRoot = simpleFind(aIndex, "green");
        controller.displaySize(aRoot, treeSize[aRoot]);

        int bRoot = simpleFind(bIndex, "green");
        controller.displaySize(bRoot, treeSize[bRoot]);

        controller.saveState(getArray());

        //Only connectNodes if they were not already in the same tree
        if (aRoot != bRoot) {
            if (treeSize[bRoot] < treeSize[aRoot]) {
                controller.connectNodes(bRoot, aRoot);
                controller.setValueAtIndex(bRoot, aRoot);
                getArray()[bRoot] = aRoot;
                treeSize[aRoot] += treeSize[bRoot];
            } else {
                controller.connectNodes(aRoot, bRoot);
                controller.setValueAtIndex(aRoot, bRoot);
                getArray()[aRoot] = bRoot;
                treeSize[bRoot] += treeSize[aRoot];
            }
        }
        delay(getDelayTime()*2);

        removeHighlighting(aRoot);
        removeHighlighting(bRoot);
        controller.setSelectedIndex(aIndex, false);
        controller.setSelectedIndex(bIndex, false);
    }

    @Override
    public void invertPause() {
        paused = !paused;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    int simpleFind(int pIndex, String color) {
        int root = pIndex;
        while (root != getArray()[root]) {
            highlightSingleNode(root, "orange");
            delay(getDelayTime());
            removeHighlighting(root);
            root = getArray()[root];
        }

        highlightSingleNode(root, color);

        return root;
    }

    @Override
    public String getName() {
        return "Weighted Union";
    }

    @Override
    public void setArray(int [] array) {
        super.setArray(array);
        refreshSizeArray(array);
    }

    @Override
    public boolean connectedNoGUIUpdate(int a, int b) {
        return getRoot(a) == getRoot(b);
    }

    /**
     * After a new array has been set (by StateControl), sizeTree needs to be refreshed
     * @param array
     */
    private void refreshSizeArray(int [] array) {
        treeSize = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int root = getRoot(array[i]);
            treeSize[root]++;
        }
    }

    @Override
    int getRoot(int index) {
        int root = index;
        while (root != getArray()[root]) { root = getArray()[root]; }
        return root;
    }
}
