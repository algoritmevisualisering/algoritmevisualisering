package algorithms.unionFind;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Class to keep track of state (used by stepper)
 */
public class State {

    private JSONArray twoDimRelationshipArray;
    private JSONArray backendArrayJSON;
    private int [] backendArray;

    public State(int [] backendArray, JSONArray twoDimRelationshipArray) {
        this.backendArray = backendArray;
        this.twoDimRelationshipArray = twoDimRelationshipArray;
        backendArrayJSON = jsonifyBackendArray(this.backendArray);
    }

    /**
     * @return The default state when a new algorithm start, used by StateController
     */
    public static State getDefaultState() {
        int [] defArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        String [][] defRelationships = {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}};

        try {
            return new State(defArray, new JSONArray(defRelationships));
        } catch (JSONException jsonEx) {
            System.out.println(jsonEx.getMessage());
            return null;
        }
    }

    private JSONArray jsonifyBackendArray(int [] backendArray) {
        String [] arr = new String[backendArray.length];
        for (int i = 0; i < backendArray.length; i++) { arr[i] = backendArray[i] + ""; }
        try {
            return new JSONArray(arr);
        } catch (JSONException jsonEx) {
            System.err.println("Error parsing backend array to JSON in State-class, " + jsonEx.getMessage());
            return null;
        }
    }

    public int [] backendArray() { return backendArray; }
    public JSONArray getTwoDimRelationshipArray() { return twoDimRelationshipArray; }
    public JSONArray backendArrayJSON() { return backendArrayJSON; }
}
