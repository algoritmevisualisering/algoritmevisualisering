package algorithms.unionFind;

/**
 * Created by KnutAnders on 02.04.2016.
 */
public class Controller {


    public enum MethodName { UNION, CONNECTED, FIND };

    private IAlgorithm algorithm;
    private MethodName methodToUse;
    private IView GUI;
    private int speed;

    public Controller(IAlgorithm algorithm) {
        this.algorithm = algorithm;
        algorithm.setController(this);
        speed = 50;
    }

    // This method is called from the view, changing the speed of the animation
    public void changeSpeed(int newSpeed) {
        speed = newSpeed;
    }

    public int getSpeed() {
        return speed;
    }

    // Called from GUI, sets the gui
    public void setGUI(IView GUI) {
        this.GUI = GUI;

    }

    // Gets the array from backend, sets algorithm
    public void setup() {
        GUI.displayArray(algorithm.getArray());
        this.setMethodToUse(MethodName.UNION);
    }

    // Connects two elements
    public void connected(int firstIndex, int secondIndex) {
        Thread t = new Thread(()-> {
            GUI.screenLock(true);
            algorithm.connected(firstIndex,secondIndex);
            GUI.screenLock(false);
        });
        t.start();
    }

    // Unions two elements
    public void union(int firstIndex, int secondIndex) {
        Thread t = new Thread(() -> {
            GUI.screenLock(true);
            algorithm.union(firstIndex,secondIndex);
            GUI.screenLock(false);
        });
        t.start();
    }

    // Finds an element from index given using the algorithm specified
    public void find(int index) {
        Thread t = new Thread() {
            @Override
            public void run() {
                GUI.screenLock(true);
                algorithm.find(index);
                GUI.screenLock(false);
            }
        };
        t.start();
    }

    // The arrow shows where the algorithm is
    public void setArrow(int index) {
        GUI.setArrow(index);
    }

    public void setSelectedIndex(int index, boolean select) {
        GUI.selectIndex(index, select);
    }

    // Changing the value to bValue on index i
    public void setValueAtIndex(int i, int bValue) {
        GUI.setValueAtIndex(i, bValue);
    }

    // Connecting nodes in GUI, called from backend
    public void connectNodes(int child, int parent) { GUI.connectNodes(child, parent); }

    public void highlightNode(int index, String color) { GUI.highlightNode(index, color); }

    // Plays or pauses the animation
    public synchronized void invertPauseState() {
        algorithm.invertPause();
    }

    // Sets a new algorithm
    public void setAlgorithm(IAlgorithm algorithm) {
        this.algorithm = algorithm;
        algorithm.setController(this);
    }

    public void removeHighlight(int node) {
        GUI.removeHighlight(node);
    }

    // Sets a new method
    public void setMethodToUse(MethodName methodToUse) {
        this.methodToUse = methodToUse;
    }

    public String getNameOfCurrentAlgorithm() {
        return algorithm.getName();
    }

    public int[] getArrayClone() {
        return algorithm.getArray().clone();
    }

    public void setArray(int[] array) {
        algorithm.setArray(array);
    }

    public void checkMark(int aIndex, int bIndex, boolean set) {
        GUI.checkMark(aIndex, bIndex, set);
    }
    public void redCross(int aIndex, int bIndex, boolean set) {
        GUI.redCross(aIndex, bIndex, set);
    }

    public void displaySize(int root, int size) {
        GUI.displayNodeSize(root, size);
    }

    public void saveState(int[] arr) {
        GUI.executeSaveMethodInJavaScript(arr.clone());
    }
}
