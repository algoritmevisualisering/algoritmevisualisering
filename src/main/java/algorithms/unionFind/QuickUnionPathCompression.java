package algorithms.unionFind;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kristian, Knut Anders, Ragnhild
 */
public class QuickUnionPathCompression extends QuickUnion {

    public QuickUnionPathCompression(int size) {
        super(size);
    }

    @Override
    public void union(int a, int b) {
        super.union(a,b);
    }

    @Override
    int simpleFind(int pIndex, String color) {
        int root = pIndex, savedIndex = pIndex;

        while(root != getArray()[root]) {
            highlightSingleNode(root, "orange");
            delay((long)(getDelayTime()*0.75));
            root = getArray()[root];
        }

        highlightSingleNode(root, color);

        List<Integer> nodeStack = new ArrayList<>();

        while (pIndex != root) {
            highlightSingleNode(pIndex, "orange");
            controller.connectNodes(pIndex, root);
            controller.setValueAtIndex(pIndex, root);
            delay((long)(getDelayTime()*1.0));
            nodeStack.add(pIndex);

            int newP = getArray()[pIndex];
            getArray()[pIndex] = root;
            pIndex = newP;
        }

        nodeStack.forEach(n -> removeHighlighting(n));

        controller.setSelectedIndex(savedIndex, true);
        highlightSingleNode(root, color);

        return root;
    }

    @Override
    public String getName() {
        return "QU w/ Path Compression";
    }

}
