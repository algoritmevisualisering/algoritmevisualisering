package algorithms.unionFind;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Stack;

/**
 *  Controller to take care of the stepping
 *  @author Kristian Rosland
 */
public class StateController {

    private Stack<State> previousStates, nextStates;
    private final Controller controller;
    private final IView view;

    public StateController(Controller controller, IView view) {
        previousStates = new Stack<>();
        nextStates = new Stack<>();
        this.controller = controller;
        this.view = view;
    }

    /**
     * Method for stepping back (if possible)
     * Also saves the current state to nextStates to be able to step forward again
     */
    public boolean stepBack(JSONArray twoDimRelationshipsJSON, JSONArray backendArrayJSON) {
        if (previousStates.isEmpty())
            return false;

        saveStateToNextStates(twoDimRelationshipsJSON, backendArrayJSON);
        State previousState = previousStates.pop();
        setState(previousState);
        return true;
    }


    /**
     * Method for stepping forward (if possible)
     * Also save the current state to previousStates to be able to step back again
     */

    public boolean stepForward(JSONArray twoDimRelationshipJSON, JSONArray backendArrayJSON) {
        if (nextStates.isEmpty())
            return false;

        saveStateToPreviousStates(twoDimRelationshipJSON, backendArrayJSON);
        State nextState = nextStates.pop();
        setState(nextState);
        return true;
    }

    /**
     * Method for saving the current state. Sent before every unionFind()-call
     */
    public void saveState(JSONArray twoDimRelationshipsJSON, JSONArray backendArrayJSON) {
        nextStates.removeAllElements();
        saveStateToPreviousStates(twoDimRelationshipsJSON, backendArrayJSON);
    }


    /** Private help methods to take care of states **/
    private void saveStateToPreviousStates(JSONArray twoDimRelationshipsJSON, JSONArray backendArrayJSON) {
        State state = getState(twoDimRelationshipsJSON, backendArrayJSON);
        previousStates.push(state);
    }

    private void saveStateToNextStates(JSONArray twoDimRelationshipsJSON, JSONArray backendArrayJSON) {
        State state = getState(twoDimRelationshipsJSON, backendArrayJSON);
        nextStates.push(state);
    }

    private State getState(JSONArray twoDimRelationshipsJSON, JSONArray backendArrayJSON) {
        JSONArray twoDimRelationship = twoDimRelationshipsJSON;
        int [] backendArray = new int[backendArrayJSON.length()];

        for (int i = 0; i < backendArray.length; i++) {
            try {
                backendArray[i] = backendArrayJSON.getInt(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return new State(backendArray, twoDimRelationship);
    }

    /**
     * Set the current state of the backend and frontend to parameter state
     * @param state
     */
    private void setState(State state) {
        view.setState(state.getTwoDimRelationshipArray(), state.backendArrayJSON());
        controller.setArray(state.backendArray());
    }
}
