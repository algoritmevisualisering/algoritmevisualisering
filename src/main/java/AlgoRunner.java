import algorithms.Graphs.DfsBfsView;
import algorithms.Graphs.GraphsStructureView;
import algorithms.QuickSortAlgorithm.QuickSortView;
import algorithms.SimpleSorting.JavascriptView;
import algorithms.unionFind.UnionFindView;
import javafx.application.Application;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * This class is created by kristianrosland on 06.09.2016.
 */
public class AlgoRunner {

    private final static String VERSION = "res1_0";

    private static Scanner scan = new Scanner(System.in);

    public static void main(String [] args) {
        String resFolderPath = "";

        // Extracting resource folders from jar file
        try {
            URI javaPath = AlgoRunner.class.getProtectionDomain().getCodeSource().getLocation().toURI();
            resFolderPath = extractJarFile(javaPath.getPath(), javaPath.resolve("" +
                    ".").getPath());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Could not get path to running .jar-file");
            System.exit(0);
        }

        // Run program...
        while(true) {
            System.out.println("Pick algorithm: \n" +
                    "1: Union Find \n" +
                    "2: Insertion/Shell sort \n" +
                    "3: Quick sort \n" +
                    "4: Graphs representation \n" +
                    "5: DFS / BFS \n" +
                    "9: Exit \n");

            new AlgoRunner().getAns(args, resFolderPath);
        }
    }

    private void getAns(String[] args, String resFolderPath) {
        try {
            int choice = Integer.parseInt(scan.nextLine());
            switch (choice) {
                case 1:
                    Application.launch(UnionFindView.class , args); break;
                case 2:
                    Application.launch(JavascriptView.class, args); break;
                case 3:
                    Application.launch(QuickSortView.class, args); break;
                case 4:
                case 5:
                    boolean didOpenInBrowser = openInBrowser(choice == 4 ? "graphs.html" : "DfsBfs.html", resFolderPath);
                    if (!didOpenInBrowser) {
                        if (choice == 4)
                            Application.launch(GraphsStructureView.class, args);
                        else if (choice == 5)
                            Application.launch(DfsBfsView.class, args);
                    }
                    break;
                case 9: System.exit(0);
            }
        } catch (NumberFormatException nfe) {
            System.out.println(nfe.getMessage());
        }
    }

    private boolean openInBrowser(String fileName, String resFolderPath) {
        File htmlFile = new File(resFolderPath + File.separator + fileName);

        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(htmlFile);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    private static String extractJarFile(String jarFile, String destDir) throws Exception {

        String rootFolder = destDir + File.separator + VERSION;
        File parentDir = new File(rootFolder);

        // Check if already extracted
        if (parentDir.exists())
            return rootFolder;

        // Make root folder
        parentDir.mkdir();
        destDir = parentDir.getPath();
        System.out.println("Unzipping jar...");
        java.util.jar.JarFile jar = new java.util.jar.JarFile(jarFile);
        java.util.Enumeration enumEntries = jar.entries();
        while (enumEntries.hasMoreElements()) {
            java.util.jar.JarEntry file = (java.util.jar.JarEntry) enumEntries.nextElement();
            java.io.File f = new java.io.File(destDir + java.io.File.separator + file.getName());
            if (file.isDirectory() || skipFile(file.getName())) { // if its a directory, create it
                continue;
            }
            f.getParentFile().mkdirs();
            java.io.InputStream is = jar.getInputStream(file); // get the input stream
            java.io.FileOutputStream fos = new java.io.FileOutputStream(f);
            while (is.available() > 0) {  // write contents of 'is' to 'fos'
                fos.write(is.read());
            }
            fos.close();
            is.close();
        }
        return destDir;
    }

    private static boolean skipFile(String name) {
        String[] validPaths = {"css", "js", "DfsBfs.html", "graphs.html", "assets"};
        for (String validPath : validPaths) {
            if (name.startsWith(validPath)) return false;
        }
        return true;
    }

}
