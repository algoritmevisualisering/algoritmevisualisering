package algorithms.QuickSort;

import algorithms.IntArrayGenerator;
import algorithms.QuickSortAlgorithm.QuickSortAlgorithm;
import algorithms.QuickSortAlgorithm.QuickSortView;
import algorithms.SimpleSorting.IsSorted;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by knutandersstokke on 06.09.2016.
 */
public class QuickSortTest {

    QuickSortAlgorithm quickSort;
    IntArrayGenerator gen = new IntArrayGenerator();
    QuickSortView view = mock(QuickSortView.class);

    @Before
    public void setup() {
        quickSort = new QuickSortAlgorithm(view);
    }

    @Test
    public void checkFor3Value() {
        Integer[] array = {3, 2, 1};
        quickSort.setArray(array);
        quickSort.sort();
        assertTrue(IsSorted.isSorted(array));
    }

    @Test
    public void checkWithSimpleArray() {
        for (int i=0; i<10; i++) {
            Integer[] array = gen.getIntegerArray(100);
            quickSort.setArray(array);
            quickSort.sort();
            assertTrue(IsSorted.isSorted(array));
        }
    }

    @Test
    public void checkWithAdvancedArray() {
        for (int i=0; i<10; i++) {
            Integer[] array = gen.getIntegerArray(1000);
            quickSort.setArray(array);
            quickSort.sort();
            assertTrue(IsSorted.isSorted(array));
        }
    }

}