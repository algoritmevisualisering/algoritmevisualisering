package algorithms.SimpleSorting;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by KnutAnders on 18.07.2016.
 */
public class EventManagerTest {

    EventManager manager;

    @Before
    public void setUp() throws Exception {
        manager = new EventManager(null); // Mocks every method which use WebEngine
        manager.setDelay(0);
    }


    @Test
    public void eventsIsExecuted() throws InterruptedException {
        FrontEndEvent e1 = mock(FrontEndEvent.class);
        FrontEndEvent e2 = mock(FrontEndEvent.class);
        manager.addEvent(e1);
        manager.addEvent(e2);
        Thread.sleep(10);
        verify(e1).execute(null);
        verify(e2).execute(null);
    }

    @Test
    public void threadWaitsAndRestartsCorrectly() throws InterruptedException {
        manager.pause();
        FrontEndEvent e = mock(FrontEndEvent.class);
        manager.addEvent(e);
        verify(e, never()).execute(null);
        manager.play();
        Thread.sleep(5);
        verify(e).execute(null);
    }

}