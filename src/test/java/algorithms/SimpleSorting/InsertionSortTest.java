package algorithms.SimpleSorting;

import algorithms.IntArrayGenerator;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static com.sun.xml.internal.ws.dump.LoggingDumpTube.Position.Before;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by KnutAnders on 09.06.2016.
 */
public class InsertionSortTest {

    private InsertionSort insSort;

    @Before
    public void setUp() {
        insSort = new InsertionSort();
        Controller controller = mock(Controller.class);
        insSort.setController(controller);
    }

    /** INSERTION SORT **/
    @Test
    public void createObjectAndInsertNewArray() {
        int[] array = {3, 5, 2, 8, 7};
        insSort.setArray(array);
        assertTrue(Arrays.equals(array, insSort.getArray()));
    }

    @Test
    public void assertThatInsertionSortingWorksSimple() {
        int[] array = {5, 4, 3, 2, 2, 1};
        insSort.setArray(array);
        insSort.insertionSort();
        assertTrue(isSorted(insSort.getArray()));
    }

    @Test
    public void assertThatInsertionSortingWorksAdvanced() {
        for (int i=0; i<3; i++) {
            int[] array = new IntArrayGenerator().getArray(new Random(), 1000);
            insSort.setArray(array);
            insSort.insertionSort();
            assertTrue(isSorted(array));
        }
    }

    /** SHELL SORT **/
    @Test
    public void assertThatShellWorksForSimpleArray() {
        int[] array = {5, 4, 3, 2, 1};
        insSort.setArray(array);
        insSort.shellSort();
        assertTrue(isSorted(array));
    }

    @Test
    public void assertThatShellSortingWorksAdvanced() {
        for (int i=0; i<3; i++) {
            int[] array = new IntArrayGenerator().getArray(new Random(), 1000);
            insSort.setArray(array);
            insSort.shellSort();
            assertTrue(isSorted(array));
        }
    }

    /** HELP METHODS **/
    private boolean isSorted(int[] array) {
        return IsSorted.isSorted(array);
    }

}